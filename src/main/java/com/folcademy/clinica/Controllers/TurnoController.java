package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/turnos") //es como es la URL del llamado
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {

        this.turnoService = turnoService;
    }
   // @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("") //vacio para q devuelva todos los almacenados
    public ResponseEntity<List<TurnoDto>> listarTodos() {
        return ResponseEntity.ok(turnoService.listarTodos());
    }
//desafio 8
    //@PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("/page") //vacio para q devuelva todos los almacenados //dif min 19
    public ResponseEntity<Page<TurnoDto>> listarTodobyPage(
            @RequestParam (name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam (name = "pageSize", defaultValue = "3") Integer pageSize,
            @RequestParam (name = "orferField", defaultValue = "id") String orderField
    ) {

        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }
    //@PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("/page/{idTurno}")
    public ResponseEntity<Page<TurnoDto>> listarUnobyPage(
            @PathVariable (name = "idTurno") Integer idturno,
            @RequestParam (name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam (name = "pageSize", defaultValue = "1") Integer pageSize
    ) {
        return ResponseEntity.ok(turnoService.listarUnobyId(idturno, pageNumber, pageSize));
    }
  // @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<TurnoDto> listarUno(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.listarUno(id));
    }
  //  @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("") //el objeto va por la URL
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity) {
        //@RequestBody recibe en el cuerpo de la URL
        // @validated controla los datos sino rechaza el llamado (caja negra)
        return ResponseEntity.ok(turnoService.agregar(entity));
    }
  //  @PreAuthorize("hasAuthority('delete_turno')")
    @DeleteMapping(value = "/{idturno}") //borrar
    public ResponseEntity<String> cancelarturno(@PathVariable (name = "idturno") Integer id){
        return ResponseEntity.ok(turnoService.cancelarturno(id));//ok
    }
  //  @PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("")  //modificar
    public ResponseEntity<TurnoDto> modificarTurno(@RequestBody @Validated TurnoDto entity){
        return ResponseEntity.ok(turnoService.modificarTurno(entity));
    }//ok
}