package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController{
    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
          this.pacienteService = pacienteService;
    }

    //@PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping("") //vacio para q devuelva todos los almacenados //dif min 19
    public ResponseEntity<List<PacienteDto>> listarTodo() {

        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    //@PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping("/page") //vacio para q devuelva todos los almacenados //dif min 19
    public ResponseEntity<Page<PacienteDto>> listarTodobyPage(
            @RequestParam (name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam (name = "pageSize", defaultValue = "3") Integer pageSize,
            @RequestParam (name = "orferField", defaultValue = "idpaciente") String orderField
    ) {

        return ResponseEntity.ok(pacienteService.listarTodoByPage(pageNumber, pageSize, orderField));
    }

    //@PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping(value = "/{idPaciente}")
    public ResponseEntity<PacienteDto> listarUno(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    //@PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping("/page/{idPaciente}")
    public ResponseEntity<Page<PacienteDto>> listarUnobyPage(
            @PathVariable (name = "idPaciente") Integer idpaciente,
            @RequestParam (name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam (name = "pageSize", defaultValue = "1") Integer pageSize
    ) {
        return ResponseEntity.ok(pacienteService.listarUnobyId(idpaciente, pageNumber, pageSize));
    }

    //@PreAuthorize("hasAuthority('post_paciente')")
    @PostMapping("") //el objeto va por la URL
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity){
        //@RequestBody recibe en el cuerpo de la URL
        // @validated controla los datos sino rechaza el llamado (caja negra)
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }
    //desafio 3
   // @PreAuthorize("hasAuthority('put_paciente')")
    @PutMapping("")  //modificar
    public ResponseEntity<PacienteDto> modificarUno(@RequestBody @Validated PacienteDto entity){
        return ResponseEntity.ok(pacienteService.modificarUno(entity));
    }//ok
   // @PreAuthorize("hasAuthority('delete_paciente')")
    @DeleteMapping(value = "/{idPaciente}") //borrar
    public ResponseEntity<String> borrarUno(@PathVariable (name = "idPaciente") Integer id){
        return ResponseEntity.ok(pacienteService.borrarUno(id));//ok
    }
}
