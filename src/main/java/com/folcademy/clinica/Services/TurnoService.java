package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turnos;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnosService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service ("turnoService")
public class TurnoService implements ITurnosService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    /*    public List<TurnoDto> listarTodos() {
            if(turnoRepository.count()<1)
                throw new NotFoundException("BD sin turnos cargados");//ok
        return turnoRepository.findAll().stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }*/
    //Desafío 8
    public List<TurnoDto> listarTodos() {
        if (turnoRepository.count() < 1)
            throw new NotFoundException("BD sin turnos cargados");//ok
        List<Turnos> turnos = (List<Turnos>) turnoRepository.findAll();
        return turnos.stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }

    public TurnoDto listarUno(Integer id){
        if(!turnoRepository.existsById(id))
            throw new ValidationException("Turno no existe");//ok
        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);
        //devuelve null sino existe
    }
    public TurnoDto agregar(TurnoDto entity){
        if(entity.getIdp() ==null|| entity.getIdm() == null){
          throw new ValidationException("Debe completar los campos paciente y medico");}//okentity.setId(null);
        entity.setFecha(LocalDate.now());
        entity.setHora(LocalTime.now());
        entity.setAtendido(false);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }
    public String cancelarturno(Integer id) {
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("Turno no encontrado - Revisar datos ingresados");//ok
        turnoRepository.deleteById(id);
        return "Id Borrado:"+id;
    }
    public TurnoDto modificarTurno(TurnoDto entity){
        if (entity.isAtendido() == true)
            throw new ValidationException ("No se puede modificar un turno YA ATENDIDO");//ok
        TurnoDto turnoAux = turnoRepository.findById(entity.getId()).orElse(null);
        turnoAux.setFecha(entity.getFecha());
        turnoAux.setHora(entity.getHora());
        turnoAux.setAtendido(entity.isAtendido());
        turnoAux.setIdm(entity.getIdm());
        turnoAux.setIdp(entity.getIdp());
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(turnoAux)));
    }
    public Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField) );
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    public Page<TurnoDto> listarUnobyId(Integer idturno, Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        return turnoRepository.findById(idturno,pageable).map(turnoMapper::entityToDto);
    }
}

