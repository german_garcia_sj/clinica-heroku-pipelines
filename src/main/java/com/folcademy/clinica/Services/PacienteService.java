package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service ("pacienteService")
public class PacienteService{
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

        public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }
    /*public List<PacienteDto> listarTodos(){
            if(pacienteRepository.count()<0)
                throw new NotFoundException("BD no cumple con la cantidad minima de pacientes diarios");//ok
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }*/
    public List<PacienteDto> listarTodos(){ //desafio 8
        if(pacienteRepository.count()<0)
            throw new NotFoundException("BD no cumple con la cantidad minima de pacientes diarios");//ok
        List<Paciente> pacientes = (List<Paciente>)  pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }
    public PacienteDto listarUno(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new ValidationException("Paciente no existe");//ok
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
        //devuelve null sino existe
    }
    public PacienteDto agregar(PacienteDto entity){
        entity.setIdpaciente(null);
         // if(entity.getNombre()==""){
          //  throw new ValidationException ("Debe completar el nombre");}//ok
        //pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
      PacienteDto pacienteAux = pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
        pacienteAux.setIdpersonas(pacienteAux.getPersonas().idpersonas);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(pacienteAux)));
    }
    //desafio 3
    public PacienteDto modificarUno(PacienteDto entity){
        //if (entity.getApellido()=="")
        if (entity.getIdpersonas()!=entity.getPersonas().idpersonas)
            throw new ValidationException ("Los id de tablas Paciente y Personas no son iguales");//ok
        PacienteDto pacienteAux = pacienteRepository.findById(entity.getIdpaciente()).orElse(null);
        pacienteAux.setIdpersonas(entity.getIdpersonas());
        //pacienteAux.setDni(entity.getDni());
        //pacienteAux.setNombre(entity.getNombre());
        //pacienteAux.setApellido(entity.getApellido());
        //pacienteAux.setTelefono(entity.getTelefono());
        pacienteAux.setDireccion(entity.getDireccion());
        pacienteAux.setPersonas(entity.getPersonas());
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(pacienteAux)));
    }

    public String borrarUno(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente No existe - No es viable la solicitud");//ok
        pacienteRepository.deleteById(id); //medicoRepository.deleteById(id);
        return "Id Borrado:"+id;
    }

    public Page<PacienteDto>  listarTodoByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }

    public Page<PacienteDto> listarUnobyId(Integer idpaciente, Integer pageNumber, Integer pageSize) {
       Pageable pageable = PageRequest.of(pageNumber,pageSize);
       return pacienteRepository.findByIdpaciente(idpaciente,pageable).map(pacienteMapper::entityToDto);
    }
}

