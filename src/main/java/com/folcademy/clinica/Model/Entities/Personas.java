package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.PersonaDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "personas")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Personas extends PersonaDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersonas", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpersonas;
    @Column(name = "dni")
    public String dni;
    @Column(name = "Nombre")
    public String nombre;
    @Column(name = "Apellido")
    public String apellido;
    @Column(name = "Telefono")
    public String telefono;



    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Personas personas = (Personas) o;
        return Objects.equals(idpersonas, personas.idpersonas);
    }

    @Override
    public int hashCode(){
        return 47971316;
    }
}

