package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico extends MedicoDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    Integer id;
    //@Column(name = "nombre")
    //String nombre = "";
    //@Column(name = "apellido")
    //String apellido = "";
    @Column(name = "profesion")
    String profesion = "";
    @Column(name = "consulta")
    int consulta = 0;
    @Column(name = "idpersonas")
    Integer idpersonas;

    //relaciones con otras entidades
    @ManyToOne(cascade = CascadeType.ALL) //
    @NotFound(action = NotFoundAction.IGNORE) //indicacion opcional//si viene si pacinte ignoralo. Puede hacerse con excepciones .EXCEPTIONS
    @JoinColumn(name = "idpersonas",referencedColumnName = "idpersonas",insertable = false,updatable = false) //une las tablas con el campo en comun
    private Personas personas;

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode(){
        return 47971316;
    }
}
