package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente extends PacienteDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
    //@Column(name = "dni")
    //public String dni;
    //@Column(name = "Nombre")
    //public String nombre;
    //@Column(name = "Apellido")
    //public String apellido;
    //@Column(name = "Telefono")
    //public String telefono;
    @Column(name = "Direccion")
    public String direccion;
    @Column(name = "idpersonas")
    Integer idpersonas;

    //relaciones con otras entidades
    @OneToOne(cascade = CascadeType.ALL) //
    @NotFound(action = NotFoundAction.IGNORE) //indicacion opcional//si viene si pacinte ignoralo. Puede hacerse con excepciones .EXCEPTIONS
    @JoinColumn(name = "idpersonas",referencedColumnName = "idpersonas",insertable = false,updatable = false) //une las tablas con el campo en comun
    private Personas personas;

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente)  o;
        return Objects.equals(idpaciente, paciente.idpaciente);
    }

    @Override
    public int hashCode(){
        return 47971316;
    }
}
