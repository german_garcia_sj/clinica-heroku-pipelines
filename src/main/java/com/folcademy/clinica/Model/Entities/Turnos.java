package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "turno")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turnos extends TurnoDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "fecha",columnDefinition = "DATE")
    //Date fecha = new Date(1900,0,1);//(int year, int month, int date); java.util.Date;
    public LocalDate fecha = LocalDate.now();
    @Column(name = "hora",columnDefinition = "TIME")
    LocalTime hora = LocalTime.now();
    @Column(name = "atendido", columnDefinition = "TINYINT")//"BOOLEAN")//"TINYINT")
    public boolean atendido; //hace la conversion implicita el framework
    @Column(name = "idpaciente",columnDefinition = "INT")
    public Integer idp = 0;
    @Column(name = "idmedico",columnDefinition = "INT")
    public Integer idm = 0;

    //relaciones con otras entidades
    @ManyToOne //el one indica un solo atributo
    @NotFound(action = NotFoundAction.IGNORE) //indicacion opcional//si viene si pacinte ignoralo. Puede hacerse con excepciones .EXCEPTIONS
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente",insertable = false,updatable = false) //une las tablas con el campo en comun
    private Paciente paciente;

    @ManyToOne //el one indica un solo atributo
    @NotFound(action = NotFoundAction.IGNORE) //indicacion opcional//si viene sin pacinte ignoralo. Puede hacerse con excepciones
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico",insertable = false,updatable = false) //une las tablas con el campo en comun
    private Medico medico;

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turnos turno = (Turnos)  o;
        return Objects.equals(id, turno.id);
    }

    @Override
    public int hashCode(){
        return 47971316;
    }
}

