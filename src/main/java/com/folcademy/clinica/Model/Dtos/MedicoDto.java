package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Personas;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data //declara todos getter y setters en una sola linea
@AllArgsConstructor //crea constructor
@NoArgsConstructor  //para enviar datos vacíos

public class MedicoDto {
    //para transmision de datos
    Integer id; //puede venir nulo
    //@NotNull    //si es nula rehcaza el llamado
    //String nombre;
    //@NotNull    //si es nula rehcaza el llamado
    //String apellido;
    @NotNull    //si es nula rehcaza el llamado
    String profesion;
    int consulta; //int no puede ser nulo
    // public MedicoDto (Integer id,String profesion, int consulta) constructor si hace falta

    Integer idpersonas;
    Personas personas;
}
