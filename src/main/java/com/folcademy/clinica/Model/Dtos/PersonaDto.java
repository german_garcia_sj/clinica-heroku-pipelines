package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //declara todos getter y setters en una sola linea
@AllArgsConstructor //crea constructor
@NoArgsConstructor  //para enviar datos vacíos
public class PersonaDto {
    Integer idpersonas;
    @NotNull
    String  dni;
    @NotNull    //si es nula rehcaza el llamado
    String nombre;
    @NotNull    //si es nula rehcaza el llamado
    String apellido;
    @NotNull    //si es nula rehcaza el llamado
    String telefono;
}