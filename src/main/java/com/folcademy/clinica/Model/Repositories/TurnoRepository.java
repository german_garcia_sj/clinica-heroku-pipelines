package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Turnos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("TurnoRepository")
/*public interface TurnoRepository extends JpaRepository<Turnos, Integer>  {*/
public interface TurnoRepository extends PagingAndSortingRepository<Turnos, Integer> {
    @Override
    Page<Turnos> findAll(Pageable pageable);

    Page<Turnos> findById(Integer id, Pageable pageable);
}


