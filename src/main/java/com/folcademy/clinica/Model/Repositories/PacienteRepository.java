package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

//@Repository ("PacienteRepository")
/*public interface PacienteRepository extends JpaRepository<Paciente, Integer> {
    Page<Paciente> findAllByApellido(String apellido, Pageable pageable);
    //Codigo para metodologia sin DTO
    //Ejecuta un SELECT * FROM Paciente where Apellido = valor apellido
    // Pageable indica indice y tamaño de pagina a traer
}*/
//Desafio 8
@Repository ("PacienteRepository")
public interface PacienteRepository extends PagingAndSortingRepository<Paciente, Integer> {
    @Override
    Page<Paciente> findAll(Pageable pageable);

    Page<Paciente> findByIdpaciente(Integer id, Pageable pageable);
}
   /* @Query("select u from User u where u.emailAddress = ?1")
    User findByEmailAddress(String emailAddress);*/