package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Medico;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("MedicoRepository")
// para desafio 8 cambiamos JpaRepository por
// public interface MedicoRepository extends JpaRepository<Medico, Integer> { //int en vez de integer
public interface MedicoRepository extends PagingAndSortingRepository <Medico, Integer> {
    @Override
    Page<Medico> findAll(Pageable pageable);

    Page<Medico> findById(Integer id, Pageable pageable);
}
