package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper { //un mapper tiene 2 funciones
    public MedicoDto entityToDto(Medico entity){
        return Optional  //caminos o desvios y devuelve algun objeto definido como opcional
                .ofNullable(entity) //verifica que el obj entity no sea nulo para seguir el flujo sino sigue en orElse
                .map(
                        ent -> new MedicoDto( //landa: nueva funcion
                                ent.getId(),    //parametros en el mismo orden que el DTO
                                //ent.getNombre(),
                                //ent.getApellido(),
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersonas(),
                                ent.getPersonas()
                        )
                )              //
                .orElse(new MedicoDto());
    }
    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico(); //ya trae cadena vacia (ver declaracion Entities Medico)
        entity.setId(dto.getId());
        //entity.setNombre(dto.getNombre());
        //entity.setApellido(dto.getApellido());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersonas(dto.getIdpersonas());
        entity.setPersonas(dto.getPersonas());
        return entity;
    }
}
