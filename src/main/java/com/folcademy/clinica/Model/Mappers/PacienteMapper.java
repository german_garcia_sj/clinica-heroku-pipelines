package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper { //un mapper tiene 2 funciones
    public PacienteDto entityToDto(Paciente entity){
        return Optional  //caminos o desvios y devuelve algun objeto definido como opcional
                .ofNullable(entity) //verifica que el obj entity no sea nulo para seguir el flujo sino sigue en orElse
                .map(
                        ent -> new PacienteDto( //landa: nueva funcion
                                ent.getIdpaciente(),    //parametros en el mismo orden que el DTO
                                //ent.getDni(),
                                //ent.getNombre(),
                                //ent.getApellido(),
                                //ent.getTelefono(),
                                ent.getDireccion(),
                                ent.getIdpersonas(),
                                ent.getPersonas()
                        )
                )              //
                .orElse(new PacienteDto());
    }
    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente(); //ya trae cadena vacia (ver declaracion Entities Medico)
        entity.setIdpaciente(dto.getIdpaciente());
        //entity.setDni(dto.getDni());
        //entity.setNombre(dto.getNombre());
        //entity.setApellido(dto.getApellido());
        //entity.setTelefono(dto.getTelefono());
        entity.setDireccion(dto.getDireccion());
        entity.setIdpersonas(dto.getIdpersonas());
        entity.setPersonas(dto.getPersonas());
        return entity;
    }
}